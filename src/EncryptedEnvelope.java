import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;



public class EncryptedEnvelope implements java.io.Serializable{
	private class EnvelopeWrapper implements java.io.Serializable{
		public Envelope env;
		public int seqnum;
		public EnvelopeWrapper(Envelope e, int snum){
			env = e;
			seqnum = snum;
		}
	}
	public byte[] iv;
	public byte[] ciphertext;

	public EncryptedEnvelope(Envelope envelope, SecretKey key, int curseq){
		Security.addProvider(new BouncyCastleProvider()); 
		try {
			SecureRandom rng = new SecureRandom();
			iv = new byte[16];
			rng.nextBytes(iv);
			EnvelopeWrapper wrapper = new EnvelopeWrapper(envelope, curseq);
			//serialize Envelope
			ByteArrayOutputStream outputstream = new ByteArrayOutputStream();
			ObjectOutputStream oOStream = new ObjectOutputStream(outputstream);
			oOStream.writeObject(wrapper);
			byte[] cleartext = outputstream.toByteArray();
			//encrypt
			Cipher aesEncrypt = Cipher.getInstance("AES/EAX/NoPadding", "BC");
			aesEncrypt.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
			ciphertext = aesEncrypt.doFinal(cleartext);
			
		}
		catch (Exception e){
		//if exception
			System.out.println("Failiure to encrypt envelope; " + e.getMessage());
			e.printStackTrace();
			
		}
	}

	public Envelope decrypt(SecretKey key, int lastSwq){
		
		Security.addProvider(new BouncyCastleProvider()); 
		try{
			Cipher aesCiph = Cipher.getInstance("AES/EAX/NoPadding", "BC");
			aesCiph.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
			byte[] cleartext = aesCiph.doFinal(ciphertext);
			ByteArrayInputStream biIS = new ByteArrayInputStream(cleartext);

			ObjectInputStream ois = new ObjectInputStream(biIS);
			
			EnvelopeWrapper wrapper =  (EnvelopeWrapper)ois.readObject();
			if (wrapper.seqnum!=(lastSwq)){
				System.out.println(wrapper.seqnum);
				System.out.println(lastSwq);

				throw new Exception("INVALID SEQUENCE");
			}
			return	wrapper.env;
		}
		catch (Exception e){
		//if exception
			System.out.println("Failiure to decrypt envelope" + e.getMessage());
			return null; 
		}
	}

}