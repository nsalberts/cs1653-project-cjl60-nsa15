/* File worker thread handles the business of uploading, downloading, and removing files for clients with valid tokens */

import java.lang.Thread;
import java.net.Socket;
import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.MessageDigest;
import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.spec.SecretKeySpec;

public class FileThread extends Thread
{
	private final Socket socket;
	private FileServer my_fs;
	private SecretKey sessionKey = null;
	private PublicKey fsPubKey;
	int snum =0;
	public FileThread(Socket _socket, FileServer myfs)
	{
		socket = _socket;
		my_fs = myfs;
	}

	public void run()
	{

		boolean proceed = true;
		try
		{
			Security.addProvider(new BouncyCastleProvider()); 
			System.out.println("*** New connection from " + socket.getInetAddress() + ":" + socket.getPort() + "***");
			final ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
			final ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
			Envelope response;
			proceed = false;

			//SESSION KEY NEGOTIATION
			//wait for request
			String req = (String)input.readObject();
			if (!(req.equals("CONNECT"))){
				System.out.println("Invalid Session Key Request");
			
			}
				//send public key
			output.writeObject(my_fs.retPubKey());
			fsPubKey = my_fs.retPubKey();

			//recieve enciphered session key
			

			byte[] sessionKeybytesEn = (byte[]) input.readObject();
			
			//decrypt
			SecretKey sessionKeyPos = my_fs.decryptSessionKey(sessionKeybytesEn);

			//confirm that session key is failed 
			Envelope confirmationEnvelope = new Envelope("SESSIONKEYCHECK");
			
			SecureRandom rng = new SecureRandom();
		
			Integer confnum1 = rng.nextInt();
			Integer confnum2 = rng.nextInt();
			
			int xor = confnum1 ^ confnum2;
			confirmationEnvelope.addObject(confnum1);
			confirmationEnvelope.addObject(confnum2);
			EncryptedEnvelope eConfEnv = new EncryptedEnvelope(confirmationEnvelope, sessionKeyPos, snum);
			snum++;
			output.writeObject(eConfEnv);
			EncryptedEnvelope recConfEnv = (EncryptedEnvelope) input.readObject();
			Envelope confEnv = recConfEnv.decrypt(sessionKeyPos, snum);
			snum++;
			
			if(((Integer)(confEnv.getObjContents().get(0))==xor)){
				System.out.println("SESSIONBUILT");
				sessionKey = sessionKeyPos;
				proceed = true;
			}
			do
			{
				Envelope e = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
				System.out.println("Request received: " + e.getMessage());
				
				// Handler to list files that this user is allowed to see
				if(e.getMessage().equals("LFILES"))
				{
				  if(e.getObjContents().size() < 1)
					{
						response = new Envelope("FAIL-BADCONTENTS");
					}
					else
					{
						if(e.getObjContents().get(0) == null) {
							response = new Envelope("FAIL-BADTOKEN");
						}
						else{
							SignedToken st = (SignedToken)e.getObjContents().get(0);
							UserToken yourToken = st.getToken();
							byte[] sig = st.getSignature();
							
							if(!yourToken.verifyToken(sig, FileServer.pubKey, FileServer.TOKEN_VALID_PERIOD, null, fsPubKey)) {
								//System.out.println("filethread token " + yourToken);
								//System.out.println("filethread sig " + sig);
								response = new Envelope("Could not verify token.");
							} 
							
							String userNm = yourToken.getSubject();
							List<String> groups = yourToken.getGroups();
							List<String> viewableFiles = new ArrayList<String>();
							for (ShareFile f: FileServer.fileList.getFiles()){
								System.out.print("Looking at :");
								System.out.println(f.getPath());
								if (f.getOwner().equals(userNm) || groups.contains(f.getGroup())){
									viewableFiles.add(f.getPath());
								}
							}

							response = new Envelope("OK");
							response.addObject(viewableFiles);
							System.out.println("Respond to Request");

						}
				    }
				    output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;
				    
				}
				else if(e.getMessage().equals("UPLOADF"))
				{

					if(e.getObjContents().size() < 5)
					{
						response = new Envelope("FAIL-BADCONTENTS");
					}
					else
					{
						if(e.getObjContents().get(0) == null) {
							response = new Envelope("FAIL-BADPATH");
						}
						if(e.getObjContents().get(1) == null) {
							response = new Envelope("FAIL-BADGROUP");
						}
						if(e.getObjContents().get(2) == null) {
							response = new Envelope("FAIL-BADTOKEN");
						}
						if(e.getObjContents().get(3) == null) {
							response = new Envelope("FAIL-BADKEYID");
						}
						if(e.getObjContents().get(4) == null) {
							response = new Envelope("FAIL-BADIV");
						}
						else {
							String remotePath = (String)e.getObjContents().get(0);
							String group = (String)e.getObjContents().get(1);
							SignedToken st = (SignedToken)e.getObjContents().get(2); //Extract token
							UserToken yourToken = st.getToken(); //Extract token
							byte[] sig = st.getSignature();
							int keyId = ((Integer)e.getObjContents().get(3)).intValue();
							// last object in envelope is the IV as a byte array but is stored as a String; read in as byte array 
							String s = (String)e.getObjContents().get(4);
							byte[] iv = s.getBytes();
							
							if(!yourToken.verifyToken(sig, FileServer.pubKey, FileServer.TOKEN_VALID_PERIOD, null, fsPubKey)) {
								response = new Envelope("Could not verify token.");
								output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;
							}
							
							else if (FileServer.fileList.checkFile(remotePath)) {
								System.out.printf("Error: file already exists at %s\n", remotePath);
								response = new Envelope("FAIL-FILEEXISTS"); //Success
							}
							else if (!yourToken.getGroups().contains(group)) {
								System.out.printf("Error: user missing valid token for group %s\n", group);
								response = new Envelope("FAIL-UNAUTHORIZED"); //Success
							}
							else  {
								File file = new File("shared_files/"+remotePath.replace('/', '_'));
								file.createNewFile();
								FileOutputStream fos = new FileOutputStream(file);
								System.out.printf("Successfully created file %s\n", remotePath.replace('/', '_'));

								response = new Envelope("READY"); //Success
								output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

								e = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
								while (e.getMessage().compareTo("CHUNK")==0) {
									fos.write((byte[])e.getObjContents().get(0), 0, (Integer)e.getObjContents().get(1));
									response = new Envelope("READY"); //Success
									output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;
									e = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
								}

								if(e.getMessage().compareTo("EOF")==0) {
									System.out.printf("Transfer successful file %s\n", remotePath);
									FileServer.fileList.addFile(yourToken.getSubject(), group, remotePath, keyId, iv);
									response = new Envelope("OK"); //Success
								}
								else {
									System.out.printf("Error reading file %s from client\n", remotePath);
									response = new Envelope("ERROR-TRANSFER"); //Success
								}
								fos.close();
							}
						}
					}

					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;
				}
				else if (e.getMessage().compareTo("DOWNLOADF")==0) {

					String remotePath = (String)e.getObjContents().get(0);
					SignedToken st = (SignedToken)e.getObjContents().get(1); //Extract token
					UserToken t = st.getToken(); //Extract token
					byte[] sig = st.getSignature();

					if (!t.verifyToken(sig, FileServer.pubKey, FileServer.TOKEN_VALID_PERIOD, null, fsPubKey)) {
						e = new Envelope("Could not verify token.");
						output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;
						
						continue;
					}
					
					ShareFile sf = FileServer.fileList.getFile("/"+remotePath);
					if (sf == null) {
						System.out.printf("Error: File %s doesn't exist\n", remotePath);
						e = new Envelope("ERROR_FILEMISSING");
						output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;

					}
					else if (!t.getGroups().contains(sf.getGroup())){
						System.out.printf("Error user %s doesn't have permission\n", t.getSubject());
						e = new Envelope("ERROR_PERMISSION");
						output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;
					}
					else {

						try
						{
							File f = new File("shared_files/_"+remotePath.replace('/', '_'));
						if (!f.exists()) {
							System.out.printf("Error file %s missing from disk\n", "_"+remotePath.replace('/', '_'));
							e = new Envelope("ERROR_NOTONDISK");
							output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;

						}
						else {
							FileInputStream fis = new FileInputStream(f);

							do {
								byte[] buf = new byte[4096];
								if (e.getMessage().compareTo("DOWNLOADF")!=0) {
									System.out.printf("Server error: %s\n", e.getMessage());
									break;
								}
								e = new Envelope("CHUNK");
								int n = fis.read(buf); //can throw an IOException
								if (n > 0) {
									System.out.printf(".");
								} else if (n < 0) {
									System.out.println("Read error");

								}


								e.addObject(buf);
								e.addObject(new Integer(n));
								e.addObject(sf.getGroup());
								e.addObject(sf.getKeyId()); // add key id so user knows which decryption key to use
								e.addObject(new String(sf.getIv())); // add IV for decryption
								

								output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;

								e = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
							}
							while (fis.available()>0);

							//If server indicates success, return the member list
							if(e.getMessage().compareTo("DOWNLOADF")==0)
							{

								e = new Envelope("EOF");
								output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;

								e = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
								if(e.getMessage().compareTo("OK")==0) {
									System.out.printf("File data upload successful\n");
								}
								else {

									System.out.printf("Upload failed: %s\n", e.getMessage());

								}

							}
							else {

								System.out.printf("Upload failed: %s\n", e.getMessage());

							}
						}
						}
						catch(Exception e1)
						{
							System.err.println("Error: " + e.getMessage());
							e1.printStackTrace(System.err);

						}
					}
				}
				else if (e.getMessage().compareTo("DELETEF")==0) {

					String remotePath = (String)e.getObjContents().get(0);
					SignedToken st = (SignedToken)e.getObjContents().get(1); //Extract token
					UserToken t = st.getToken(); //Extract token
					byte[] sig = st.getSignature();
					
					if (!t.verifyToken(sig, FileServer.pubKey, FileServer.TOKEN_VALID_PERIOD, null, fsPubKey)) {
						e = new Envelope("Could not verify token.");
						output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;
						continue;
					}
					
					ShareFile sf = FileServer.fileList.getFile("/"+remotePath);
					if (sf == null) {
						System.out.printf("Error: File %s doesn't exist\n", remotePath);
						e = new Envelope("ERROR_DOESNTEXIST");
					}
					else if (!t.getGroups().contains(sf.getGroup())){
						System.out.printf("Error user %s doesn't have permission\n", t.getSubject());
						e = new Envelope("ERROR_PERMISSION");
					}
					else {

						try
						{


							File f = new File("shared_files/"+"_"+remotePath.replace('/', '_'));

							if (!f.exists()) {
								System.out.printf("Error file %s missing from disk\n", "_"+remotePath.replace('/', '_'));
								e = new Envelope("ERROR_FILEMISSING");
							}
							else if (f.delete()) {
								System.out.printf("File %s deleted from disk\n", "_"+remotePath.replace('/', '_'));
								FileServer.fileList.removeFile("/"+remotePath);
								e = new Envelope("OK");
							}
							else {
								System.out.printf("Error deleting file %s from disk\n", "_"+remotePath.replace('/', '_'));
								e = new Envelope("ERROR_DELETE");
							}


						}
						catch(Exception e1)
						{
							System.err.println("Error: " + e1.getMessage());
							e1.printStackTrace(System.err);
							e = new Envelope(e1.getMessage());
						}
					}
					output.writeObject(new EncryptedEnvelope(e, sessionKey, snum)); snum++;

				}
				else if(e.getMessage().equals("DISCONNECT"))
				{
					socket.close();
					proceed = false;
				}
			} while(proceed);
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
}
