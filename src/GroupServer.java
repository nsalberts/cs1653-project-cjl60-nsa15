/* Group server. Server loads the users from UserList.bin.
 * If user list does not exists, it creates a new list and makes the user the server administrator.
 * On exit, the server saves the user list to file.
 */

/*
 * TODO: This file will need to be modified to save state related to
 *       groups that are created in the system
 *
 */

import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;
import java.util.*;


import javax.crypto.Cipher;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import javax.crypto.BadPaddingException;

public class GroupServer extends Server implements java.io.Serializable {

	public static final int SERVER_PORT = 8765;
	private final int RSA_KEY_SIZE = 2048;
	private int AES_KEY_SIZE = 128; // there is a limit on key size depending on what version of java is used; default to 128 but call method to set to max allowed size
	private final String SECURE_R_ALGO = "SHA1PRNG";
	private final String SIGNING_ALGO = "SHA256withRSA";
	private final String HASH_ALGO = "SHA-256";
	public static final long TOKEN_VALID_PERIOD = 600000;
	private KeyPair keys;
	private final int SALT_LENGTH = 128; // in bytes
	
	public UserList userList;
	public Hashtable<String, Group> groupList;
	public Hashtable<String, ArrayList<SecretKeySpec>> groupKeys; // group name --> history of keys; all groups contained here
	
	public GroupServer() {
		super(SERVER_PORT, "ALPHA");
	}

	public GroupServer(int _port) {
		super(_port, "ALPHA");
	}

	public void setAesKeySize() throws NoSuchAlgorithmException {
		AES_KEY_SIZE = Cipher.getMaxAllowedKeyLength("AES");
	}
	
	public void start() throws NoSuchProviderException, NoSuchAlgorithmException {
		Security.addProvider(new BouncyCastleProvider()); // add bouncycastle as a security provider
	
		setAesKeySize();
		
		// Overwrote server.start() because if no user file exists, initial admin account needs to be created
		String userFile = "UserList.bin";
		String groupFile = "GroupList.bin";
		String keyFile = "GSKeys.bin";
		String groupKeysFile = "GroupKeys.bin";
		
		Scanner console = new Scanner(System.in);
		ObjectInputStream userStream;
		ObjectInputStream groupStream;
		ObjectInputStream keyStream;
		ObjectInputStream groupKeysStream;
		
		//This runs a thread that saves the lists on program exit
		Runtime runtime = Runtime.getRuntime();
		runtime.addShutdownHook(new ShutDownListener(this));

		//Open key file to get the server's keys
		try
		{
			keyStream = new ObjectInputStream(new FileInputStream(keyFile));
			keys = (KeyPair)keyStream.readObject();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Keys File Does Not Exist. Generating keys...");
			
			//Generate a KeyPair for this server
			keys = generateKeyPair();
			System.out.println("Generated keys!");
			//Save KeyPair to file for future use
			try {
				ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(keyFile));
				outStream.writeObject(keys);
				System.out.println("Saved keys!");
			} catch (IOException ex) {
				System.out.println("IO Error writing to Keys file");
			} 
		}
		catch(IOException e)
		{
			System.out.println("IO Error reading from Keys file");
			e.printStackTrace();
			System.exit(-1);
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("ClassNotFound Error reading from Keys file");
			System.exit(-1);
		}
		
		//Open group file to get group list
		try
		{
			FileInputStream fis2 = new FileInputStream(groupFile);
			groupStream = new ObjectInputStream(fis2);
			groupList = (Hashtable<String, GroupServer.Group>)groupStream.readObject();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("GroupList File Does Not Exist. Creating GroupList...");
			
			//Create a new list
			groupList = new Hashtable<String, Group>();
		}
		catch(IOException e)
		{
			System.out.println("IO Error reading from GroupList file");
			e.printStackTrace();
			System.exit(-1);
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("ClassNotFound Error reading from GroupList file");
			System.exit(-1);
		}
		
		//Open groupKeys file to get list of groups' keys
		try
		{
			FileInputStream fis3 = new FileInputStream(groupKeysFile);
			groupKeysStream = new ObjectInputStream(fis3);
			groupKeys = (Hashtable<String, ArrayList<SecretKeySpec>>)groupKeysStream.readObject();
			groupKeysStream.close();
			fis3.close();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("No group keys found; creating new table of group keys...");
			
			//Create a new Hashtable for group keys
			groupKeys = new Hashtable<String, ArrayList<SecretKeySpec>>();
		}
		catch(IOException e)
		{
			System.out.println("IO Error reading from GroupKeys file");
			e.printStackTrace();
			System.exit(-1);
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("ClassNotFound Error reading from GroupKeys file");
			System.exit(-1);
		}
		
		//Open user file to get user list
		try
		{
			FileInputStream fis = new FileInputStream(userFile);
			userStream = new ObjectInputStream(fis);
			userList = (UserList)userStream.readObject();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("UserList File Does Not Exist. Creating UserList...");
			System.out.println("No users currently exist. Your account will be the administrator.");
			System.out.print("Enter your username: ");
			String username = console.next();
			
			//Create a new list, add current user to the ADMIN group. They now own the ADMIN group.
			userList = new UserList();
			
			// Get password
			char[] pw1;
			char[] pw2;
			Console con = System.console();
				
			do {
				System.out.print("Enter a password: ");
				pw1 = con.readPassword();
				System.out.print("Retype your password: ");
				pw2 = con.readPassword();
				if (!Arrays.equals(pw1, pw2)) {
					System.out.println("Error -- passwords do not match. Please enter again.");
				}
			} while (!Arrays.equals(pw1, pw2)); // make sure passwords match
			
			// Generate random salt
			SecureRandom r = new SecureRandom();
			byte[] salt = new byte[128]; // make salt of size 128
			r.nextBytes(salt);
			
			// Hash password for this user 
			MessageDigest hash = MessageDigest.getInstance(HASH_ALGO);
			hash.update((new String(pw1)).getBytes());
			hash.update(salt);
			byte[] hashedPwd = hash.digest(); 
					
			// Add user
			userList.addUser(username, hashedPwd, salt);
			if (addGroup(username, "ADMIN")) { // add to table of groups
				userList.addGroup(username, "ADMIN");
				userList.addOwnership(username, "ADMIN");
			} else {
				System.out.println("error -- need unique groupname");
			}
			
			System.out.println("Created administrator!");
		}
		catch(IOException e)
		{
			System.out.println("Error reading from UserList file");
			System.exit(-1);
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Error reading from UserList file");
			System.exit(-1);
		}

		
		
		getPublicKey();
		//Autosave Daemon. Saves lists every 5 minutes
		AutoSave aSave = new AutoSave(this);
		aSave.setDaemon(true);
		aSave.start();

		//This block listens for connections and creates threads on new connections
		try
		{

			final ServerSocket serverSock = new ServerSocket(port);

			Socket sock = null;
			GroupThread thread = null;

			while(true)
			{
				sock = serverSock.accept();
				thread = new GroupThread(sock, this);
				thread.start();
			}
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}

	}

	public PublicKey retPubKey(){
		return keys.getPublic();
	}
	public SecretKey decryptSessionKey(byte[] ciphertext) throws NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException {
		Cipher rsaDecrypt = Cipher.getInstance("RSA/ECB/OAEPPADDING", "BC");
		rsaDecrypt.init(Cipher.DECRYPT_MODE, keys.getPrivate());
		
		byte[] sessionKeybytes = rsaDecrypt.doFinal(ciphertext);
		//posible session key
		SecretKey sessionKeyPos = new SecretKeySpec(sessionKeybytes, "AES");
		return sessionKeyPos;
			
	}

	private KeyPair generateKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
		// Generate public/private keys for RSA
		SecureRandom random = SecureRandom.getInstance(SECURE_R_ALGO); // get random number
		
		// Create key generator
		KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA", "BC");
		keygen.initialize(RSA_KEY_SIZE, random);
		
		// Generator key pair
		KeyPair kp = keygen.generateKeyPair();
		
		return kp;
	}
	
	private SecretKeySpec generateGroupKey() throws NoSuchAlgorithmException, NoSuchProviderException {
		// Generate AES key for group encryption/decryption
		SecureRandom random = SecureRandom.getInstance(SECURE_R_ALGO); // get random number
		
		// generate key
		KeyGenerator kg = KeyGenerator.getInstance("AES", "BC"); 
		kg.init(AES_KEY_SIZE, random);
		SecretKeySpec key = (SecretKeySpec)kg.generateKey(); 
		
		return key;
	}
	
	public synchronized ArrayList<SecretKeySpec> getGroupKeys(String groupname) {
		return groupKeys.get(groupname);
	}
	
	public byte[] signToken(UserToken token) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException, ClassNotFoundException {
		// generate signature
		Signature s = Signature.getInstance(SIGNING_ALGO, "BC");
		SecureRandom r = SecureRandom.getInstance(SECURE_R_ALGO);
		s.initSign(keys.getPrivate(), r);

		// convert token to byte array
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		ObjectOutputStream outStream = new ObjectOutputStream(bOut);
		outStream.writeObject(token);
		byte[] input = bOut.toByteArray();
		
		// sign token with server's private key
		s.update(input);
		byte[] signed = s.sign();

		return signed; // return signature
	}
	


	public static void getPublicKey() {
		// This function creates a file with the group server's public key in it 
		String keyFile = "GSKeys.bin";
		try // try to read in keys
		{
			ObjectInputStream keyStream = new ObjectInputStream(new FileInputStream(keyFile));
			KeyPair keys = (KeyPair)keyStream.readObject();
			
			// Write out only the public key 
			try {
				ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream("GSPub.bin"));
				outStream.writeObject(keys.getPublic());
				System.out.println("GSPub.bin created!");
			} catch (IOException ex) {
				System.out.println("IO Error writing to public key file");
			} 
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Error -- group server has not yet been started and does not have its key pair.");
		}
		catch(IOException e)
		{
			System.out.println("IO Error reading from Keys file");
			e.printStackTrace();
			System.exit(-1);
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("ClassNotFound Error reading from Keys file");
			System.exit(-1);
		}
	}
	
	public synchronized boolean addGroup(String user, String groupname) throws NoSuchAlgorithmException, NoSuchProviderException {
		if (groupList.containsKey(groupname)) {
			return false; // need unique group name
		}
		// Add to list of groups
		Group g = new Group(groupname);
		g.addMember(user);
		groupList.put(groupname, g);
		// Generate key and add to list of group keys
		SecretKeySpec s = generateGroupKey();
		if (s!=null && groupKeys!=null) {
			groupKeys.put(groupname, new ArrayList<SecretKeySpec>()); // add group to list
			groupKeys.get(groupname).add(s); // add key to group's list of keys
		} else {
			System.out.println("failed to generate group key when adding group");
			return false;
		}
		return true;
	}
	
	public synchronized void removeGroup(String user, String groupname) {
		// get members in that group and remove all
		Group group = groupList.get(groupname);
		if (group!=null) {
			List<String> members = group.listMembers();
			for (String m : members) {
				if (userList.checkUser(m) && userList.getUserGroups(m).contains(groupname)) { // if user has this group in list of groups they're in
					userList.removeGroup(m, groupname); // remove
				}
			}
		}
		
		// check ownership
		if(userList.checkUser(user)) {
			ArrayList<String> ownedGroups = userList.getUserOwnership(user);
			for (String g : ownedGroups) {
				if (g.equals(groupname)) { // if user owns the group to be removed, can remove
					groupList.remove(g);
				}
			}
		}
		
		// Remove group from list of group keys
		groupKeys.remove(groupname);
	}
	
	public synchronized void addUserToGroup(String username, String groupname) {
		// find group in grouplist
		Group g = groupList.get(groupname);
		if (g!=null && !g.checkMember(username)) {
			g.addMember(username);
		} 
	}
	
	public synchronized void deleteUserFromGroup(String username, String groupname) throws NoSuchAlgorithmException, NoSuchProviderException {
		// find group in grouplist
		Group g = groupList.get(groupname);
		if (g!=null && g.checkMember(username)) {
			g.removeMember(username);
			
			// generate new key for that group
			SecretKeySpec s = generateGroupKey();
			if (s!=null) {
				groupKeys.get(groupname).add(s); // add key to group's list of keys
			}
		} 
	}
	
	static class Group implements java.io.Serializable {
		public String groupName;
		private List<String> users;
		
		public Group() {
			groupName = null;
			users = new ArrayList<String>();
		}
		
		public Group(String name) {
			groupName = name;
			users = new ArrayList<String>();
		}
		
		public boolean checkMember(String user){
			for (String u : users) {
				if (u.equals(user)) {
					return true;
				}
			}
			return false;
		}
		
		public List<String> listMembers() {
			return users;
		}
		
		public void addMember(String user) {
			users.add(user);
		}
		
		public void removeMember(String user) {
			for (String u : users) { // find the user
				if (u.equals(user)) {
					users.remove(u);
					break;
				}
			}
		}
	}
}

//This thread saves the user list
class ShutDownListener extends Thread
{
	public GroupServer my_gs;

	public ShutDownListener (GroupServer _gs) {
		my_gs = _gs;
	}

	public void run()
	{
		System.out.println("Shutting down server");
		ObjectOutputStream outStream;
		ObjectOutputStream outStream2;
		ObjectOutputStream outStream3;
		try
		{
			outStream = new ObjectOutputStream(new FileOutputStream("UserList.bin"));
			outStream.writeObject(my_gs.userList);
			outStream2 = new ObjectOutputStream(new FileOutputStream("GroupList.bin"));
			outStream2.writeObject(my_gs.groupList);
			outStream3 = new ObjectOutputStream(new FileOutputStream("GroupKeys.bin"));
			outStream3.writeObject(my_gs.groupKeys);
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
}

class AutoSave extends Thread
{
	public GroupServer my_gs;

	public AutoSave (GroupServer _gs) {
		my_gs = _gs;
	}

	public void run()
	{
		do
		{
			try
			{
				Thread.sleep(300000); //Save group and user lists every 5 minutes
				System.out.println("Autosave group and user lists...");
				ObjectOutputStream outStream;
				ObjectOutputStream outStream2;
				ObjectOutputStream outStream3;
				try
				{
					outStream = new ObjectOutputStream(new FileOutputStream("UserList.bin"));
					outStream.writeObject(my_gs.userList);
					outStream2 = new ObjectOutputStream(new FileOutputStream("GroupList.bin"));
					outStream2.writeObject(my_gs.groupList);
					outStream3 = new ObjectOutputStream(new FileOutputStream("GroupKeys.bin"));
					outStream3.writeObject(my_gs.groupKeys);
				}
				catch(Exception e)
				{
					System.err.println("Error: " + e.getMessage());
					e.printStackTrace(System.err);
				}

			}
			catch(Exception e)
			{
				System.out.println("Autosave Interrupted");
			}
		} while(true);
	}
}
