/* FileServer loads files from FileList.bin.  Stores files in shared_files directory. */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import java.security.Key;
import java.security.PublicKey;
import java.security.Security;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.security.*;
import javax.crypto.BadPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class FileServer extends Server {
	
	public static final int SERVER_PORT = 4321;
	public static final long TOKEN_VALID_PERIOD = 600000; // how long a UserToken is valid for, in milliseconds. 600,000 ms = 10 minutes
	private final String SECURE_R_ALGO = "SHA1PRNG";
	private final int RSA_KEY_SIZE = 2048;
	public static FileList fileList;
	private static String publicKeyFile; // file that contains group server's public key
	public static PublicKey pubKey; // group server's public key
	private static KeyPair fsKeys;

	public FileServer() {
		super(SERVER_PORT, "FilePile");
	}

	public FileServer(int _port) {
		super(_port, "FilePile");
	}
	
	public FileServer(String keyFile) {
		super(SERVER_PORT, "FilePile");
		this.publicKeyFile = keyFile;
	}
	
	public FileServer(int _port, String keyFile) {
		super(_port, "FilePile");
		this.publicKeyFile = keyFile;
	}
	
	public void start() throws NoSuchProviderException, NoSuchAlgorithmException{
		Security.addProvider(new BouncyCastleProvider());
		String keyPFile = "FSKeys.bin";
		String fileFile = "FileList.bin";
		ObjectInputStream fileStream;
		ObjectInputStream keyStream;
		//This runs a thread that saves the lists on program exit
		Runtime runtime = Runtime.getRuntime();
		Thread catchExit = new Thread(new ShutDownListenerFS());
		runtime.addShutdownHook(catchExit);
		
		String keyFile = "FSKeys.bin";

		//Open user file to get user list
		try
		{
			FileInputStream fis = new FileInputStream(fileFile);
			fileStream = new ObjectInputStream(fis);
			fileList = (FileList)fileStream.readObject();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("FileList Does Not Exist. Creating FileList...");
			
			fileList = new FileList();
			
		}
		catch(IOException e)
		{
			System.out.println("Error reading from FileList file");
			System.exit(-1);
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Error reading from FileList file");
			System.exit(-1);
		}
		
		File file = new File("shared_files");
		 if (file.mkdir()) {
			 System.out.println("Created new shared_files directory");
		 }
		 else if (file.exists()){
			 System.out.println("Found shared_files directory");
		 }
		 else {
			 System.out.println("Error creating shared_files directory");				 
		 }
		//Load File Server Key Pair
		 try
		{
			keyStream = new ObjectInputStream(new FileInputStream(keyPFile));
			fsKeys = (KeyPair)keyStream.readObject();
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Keys File Does Not Exist. Generating keys...");
			
			//Generate a KeyPair for this server
			fsKeys = generateKeyPair();
			System.out.println("Generated keys!");
			//Save KeyPair to file for future use
			try {
				ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(keyPFile));
				outStream.writeObject(fsKeys);
				System.out.println("Saved keys!");
			} catch (IOException ex) {
				System.out.println("IO Error writing to Keys file");
			} 
		}
		catch (IOException ex){
			System.out.println("Keys File Does Not Exist. Generating keys...");
			
			//Generate a KeyPair for this server
			fsKeys = generateKeyPair();
			System.out.println("Generated keys!");
			//Save KeyPair to file for future use
			try {
				ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(keyPFile));
				outStream.writeObject(fsKeys);
				System.out.println("Saved keys!");
			} catch (IOException exc) {
				System.out.println("IO Error writing to Keys file");
			} 
		}
		catch (ClassNotFoundException cnfe){
			System.out.println("Error Parsing keyFile");
			fsKeys = generateKeyPair();
			System.out.println("Generated keys!");
			//Save KeyPair to file for future use
			try {
				ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream(keyPFile));
				outStream.writeObject(fsKeys);
				System.out.println("Saved keys!");
			} catch (IOException exc) {
				System.out.println("IO Error writing to Keys file");
			} 
		}

		// Read group server key from publicKeyFile
		try {
			keyStream = new ObjectInputStream(new FileInputStream(publicKeyFile));
			pubKey = (PublicKey)keyStream.readObject(); // get key
		} catch(FileNotFoundException e) {
			System.out.println("Error - Public Key File Does Not Exist. Run file server again.");
			System.exit(-1);
		} catch(IOException e) {
			System.out.println("IO Error reading from public key file");
			e.printStackTrace();
			System.exit(-1);
		} catch(ClassNotFoundException e) {
			System.out.println("ClassNotFound Error reading from public key file");
			System.exit(-1);
		}
		
		//Autosave Daemon. Saves lists every 5 minutes
		AutoSaveFS aSave = new AutoSaveFS();
		aSave.setDaemon(true);
		aSave.start();
		
		boolean running = true;
		
		try
		{			
			final ServerSocket serverSock = new ServerSocket(port);
			System.out.printf("%s up and running\n", this.getClass().getName());
			
			Socket sock = null;
			Thread thread = null;
			
			while(running)
			{
				sock = serverSock.accept();
				thread = new FileThread(sock, this);
				thread.start();
			}
			
			System.out.printf("%s shut down\n", this.getClass().getName());
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
	public PublicKey retPubKey(){
		return fsKeys.getPublic();
	}
	public SecretKey decryptSessionKey(byte[] ciphertext) throws NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException {
		Cipher rsaDecrypt = Cipher.getInstance("RSA/ECB/OAEPPADDING", "BC");
		rsaDecrypt.init(Cipher.DECRYPT_MODE, fsKeys.getPrivate());
		
		byte[] sessionKeybytes = rsaDecrypt.doFinal(ciphertext);
		//posible session key
		SecretKey sessionKeyPos = new SecretKeySpec(sessionKeybytes, "AES");
		return sessionKeyPos;
			
	}

	//generate keypair for secure socket use
	private KeyPair generateKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
		// Generate public/private keys for RSA
		SecureRandom random = SecureRandom.getInstance(SECURE_R_ALGO); // get random number
		
		// Create key generator
		KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA", "BC");
		keygen.initialize(RSA_KEY_SIZE, random);
		
		// Generator key pair
		KeyPair kp = keygen.generateKeyPair();
		
		return kp;
	}
}



//This thread saves user and group lists
class ShutDownListenerFS implements Runnable
{
	public void run()
	{
		System.out.println("Shutting down server");
		ObjectOutputStream outStream;

		try
		{
			outStream = new ObjectOutputStream(new FileOutputStream("FileList.bin"));
			outStream.writeObject(FileServer.fileList);
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
}

class AutoSaveFS extends Thread
{
	public void run()
	{
		do
		{
			try
			{
				Thread.sleep(300000); //Save group and user lists every 5 minutes
				System.out.println("Autosave file list...");
				ObjectOutputStream outStream;
				try
				{
					outStream = new ObjectOutputStream(new FileOutputStream("FileList.bin"));
					outStream.writeObject(FileServer.fileList);
				}
				catch(Exception e)
				{
					System.err.println("Error: " + e.getMessage());
					e.printStackTrace(System.err);
				}

			}
			catch(Exception e)
			{
				System.out.println("Autosave Interrupted");
			}
		}while(true);
	}
}
