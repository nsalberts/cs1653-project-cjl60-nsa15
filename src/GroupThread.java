/* This thread does all the work. It communicates with the client through Envelopes.
 * 
 */
import java.lang.Thread;
import java.net.Socket;
import java.io.*;
import java.util.*;
import java.sql.Timestamp;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.MessageDigest;
import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.util.UUID;

public class GroupThread extends Thread 
{
	private final Socket socket;
	private GroupServer my_gs;
	private static final String HASH_ALGO = "SHA-256";
	private final int CHALLENGE_SIZE = 64;
	private SecretKey sessionKey = null;
	private int snum = 0;
	private String sessionId;
	private PublicKey gsPubKey;
	public GroupThread(Socket _socket, GroupServer _gs)
	{
		socket = _socket;
		my_gs = _gs;
		//generate uuid
		SecureRandom sr = new SecureRandom();
		long uuidL = sr.nextLong();
		long uuidM = sr.nextLong();
		sessionId = new UUID(uuidM, uuidL).toString();
		gsPubKey = my_gs.retPubKey();
	}
	
	public void run()
	{
		

		try
		{
			Security.addProvider(new BouncyCastleProvider()); 
			//Announces connection and opens object streams
			
			System.out.println(gsPubKey.getEncoded());
			System.out.println("*** New connection from " + socket.getInetAddress() + ":" + socket.getPort() + "***");
			final ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
			final ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
			boolean proceed = false;

			//SESSION KEY NEGOTIATION
			//wait for request
			String req = (String)input.readObject();
			if (!(req.equals("CONNECT"))){
				System.out.println("Invalid Session Key Request");
			
			}
				//send public key
			output.writeObject(my_gs.retPubKey());
			

			//recieve enciphered session key
			

			byte[] sessionKeybytesEn = (byte[]) input.readObject();
			
			//decrypt
			SecretKey sessionKeyPos = my_gs.decryptSessionKey(sessionKeybytesEn);

			//confirm that session key is failed 
			Envelope confirmationEnvelope = new Envelope("SESSIONKEYCHECK");
			
			SecureRandom rng = new SecureRandom();
		
			Integer confnum1 = rng.nextInt();
			Integer confnum2 = rng.nextInt();
			
			int xor = confnum1 ^ confnum2;
			confirmationEnvelope.addObject(confnum1);
			confirmationEnvelope.addObject(confnum2);
			EncryptedEnvelope eConfEnv = new EncryptedEnvelope(confirmationEnvelope, sessionKeyPos, snum);
			snum++;
			output.writeObject(eConfEnv);
			EncryptedEnvelope recConfEnv = (EncryptedEnvelope) input.readObject();
			Envelope confEnv = recConfEnv.decrypt(sessionKeyPos, snum);
			snum++;
			
			if(((Integer)(confEnv.getObjContents().get(0))==xor)){
				System.out.println("SESSIONBUILT");
				sessionKey = sessionKeyPos;
				proceed = true;
			}

			do
			{
				Envelope message = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
				System.out.println("Request received: " + message.getMessage());
				Envelope response;
				
				if(message.getMessage().equals("GET"))//Client wants a token
				{
					String username = (String)message.getObjContents().get(0); //Get the username
					PublicKey fsPk = (PublicKey)message.getObjContents().get(1);
					if(username == null || my_gs.userList.checkUser(username)==false)
					{ // if user does not exist, fails
						response = new Envelope("FAIL");
						response.addObject(null);
						output.writeObject(new EncryptedEnvelope(response, sessionKey, snum));
						snum++;
					}
					else
					{
						// Send client a challenge and the user's salt 
						response = new Envelope("CHALLENGE-SALT");
						// Generate random challenge
						SecureRandom r = new SecureRandom();
						byte[] challenge = new byte[CHALLENGE_SIZE]; 
						r.nextBytes(challenge);
						response.addObject(challenge);
						// Get the user's salt
						byte[] salt = my_gs.userList.getSalt(username);
						response.addObject(salt);
						// Send back to client
						output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;
						output.reset();
						
						// Get message back from client; should be challenge hashed with the hashed salted password
						
						message = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
						byte[] challengeResponse = (byte[])message.getObjContents().get(0);
						
						// Compute the same thing and compare
						boolean verified = my_gs.userList.comparePassword(username, challenge, challengeResponse);
			
						// If no match, return null
						if (verified==false) {
							response = new Envelope("FAIL");
							response.addObject(null);
							output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

							continue;
						}
						
						// Else if match, return token 
						UserToken yourToken = createToken(username, fsPk); //Create a token
						Keychain kc = createKeychain(username); // get user's group keys
						
						if (yourToken==null) { // if couldn't create token because user does not exist
							response = new Envelope("FAIL");
							response.addObject(null);
							output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

						} else {
							//Respond to the client. On error, the client will receive a null token
							response = new Envelope("OK");

							byte[] signature = my_gs.signToken(yourToken); // get signature for the token
							
							if (signature==null) { // if token signing failed 
								System.out.println("Failed to sign token, returning null");
								response = new Envelope("FAIL"); // fail to send token
								response.addObject(null);
								output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

							} else {
								// add token signature to the envelope
								SignedToken st = new SignedToken(yourToken, signature);
								response = new Envelope("OK");
								response.addObject(st);
								// add user's keychain to the envelope
								response.addObject(kc);
								output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

							}
						}
					}
				}
				else if(message.getMessage().equals("CUSER")) //Client wants to create a user
				{

					if(message.getObjContents().size() < 3)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								String username = (String)message.getObjContents().get(0); //Extract the username
								SignedToken st = (SignedToken)message.getObjContents().get(1);
								UserToken yourToken = st.getToken();
								byte[] sig = st.getSignature();
								if(!yourToken.verifyToken(sig, gsPubKey, GroupServer.TOKEN_VALID_PERIOD, sessionId, null)) {
									response = new Envelope("FAIL");
								}
								byte[] pwd = (byte[])message.getObjContents().get(2); //Extract the password
								if(createUser(username, yourToken, pwd))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
				else if(message.getMessage().equals("DUSER")) //Client wants to delete a user
				{
					
					if(message.getObjContents().size() < 2)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								String username = (String)message.getObjContents().get(0); //Extract the username
								SignedToken st = (SignedToken)message.getObjContents().get(1);
								UserToken yourToken = st.getToken();
								byte[] sig = st.getSignature();
								if(!yourToken.verifyToken(sig, gsPubKey, GroupServer.TOKEN_VALID_PERIOD, sessionId, null)) {
									response = new Envelope("FAIL");
								}
								if(deleteUser(username, yourToken))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
				else if(message.getMessage().equals("CGROUP")) //Client wants to create a group
				{
					if(message.getObjContents().size() < 2) {
						response = new Envelope("FAIL");
					} else {
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								String groupname = (String)message.getObjContents().get(0); //Extract the group name
								SignedToken st = (SignedToken)message.getObjContents().get(1);
								UserToken yourToken = st.getToken();
								byte[] sig = st.getSignature();
								if(!yourToken.verifyToken(sig, gsPubKey, GroupServer.TOKEN_VALID_PERIOD, sessionId, null)) {
									response = new Envelope("FAIL");
								}
								if(createGroup(groupname, yourToken))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
				else if(message.getMessage().equals("DGROUP")) //Client wants to delete a group
				{
				    if(message.getObjContents().size() < 2) {
						response = new Envelope("FAIL");
					} else {
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								String groupname = (String)message.getObjContents().get(0); //Extract the group name
								SignedToken st = (SignedToken)message.getObjContents().get(1);
								UserToken yourToken = st.getToken();
								byte[] sig = st.getSignature();
								if(!yourToken.verifyToken(sig, gsPubKey, GroupServer.TOKEN_VALID_PERIOD, sessionId, null)) {
									response = new Envelope("FAIL");
								}
								if(deleteGroup(groupname, yourToken))
								{
									response = new Envelope("OK"); //Success
								}
							}
						}
					}
					
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
				else if(message.getMessage().equals("LMEMBERS")) //Client wants a list of members in a group
				{
				    if(message.getObjContents().size() < 2) {
						response = new Envelope("FAIL");
					} else {
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								String groupname = (String)message.getObjContents().get(0); //Extract the group name
								SignedToken st = (SignedToken)message.getObjContents().get(1);
								UserToken yourToken = st.getToken();
								byte[] sig = st.getSignature();
								
								
								List<String> memberList = listMembers(groupname, yourToken);
								if(!yourToken.verifyToken(sig, gsPubKey, GroupServer.TOKEN_VALID_PERIOD, sessionId, null)) {
									response = new Envelope("FAIL");
								}
								if(memberList!=null)
								{
									response = new Envelope("OK"); //Success
									response.addObject(memberList);
								} else {
									response = new Envelope("FAIL");
								}
							} else {
								response = new Envelope("FAIL");
							}
						} else {
							response = new Envelope("FAIL");
						}
					}
					
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
				else if(message.getMessage().equals("AUSERTOGROUP")) //Client wants to add user to a group
				{
					if(message.getObjContents().size() < 3)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								if(message.getObjContents().get(2) != null) {
									String username = (String)message.getObjContents().get(0); //Extract the username
									String groupname = (String)message.getObjContents().get(1); //Extract the group name
									SignedToken st = (SignedToken)message.getObjContents().get(2);
									UserToken userToAdd = st.getToken();
									byte[] sig = st.getSignature();

									
									if(!userToAdd.verifyToken(sig, gsPubKey, GroupServer.TOKEN_VALID_PERIOD, sessionId, null)) {
										response = new Envelope("FAIL");
									}
									if(addUserToGroup(username, groupname, userToAdd))
									{
										response = new Envelope("OK"); //Success
									}
								}
							}
						}
					}
					
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
				else if(message.getMessage().equals("RUSERFROMGROUP")) //Client wants to remove user from a group
				{
				    if(message.getObjContents().size() < 3)
					{
						response = new Envelope("FAIL");
					}
					else
					{
						response = new Envelope("FAIL");
						
						if(message.getObjContents().get(0) != null)
						{
							if(message.getObjContents().get(1) != null)
							{
								if(message.getObjContents().get(2) != null) {
									String username = (String)message.getObjContents().get(0); //Extract the username
									String groupname = (String)message.getObjContents().get(1); //Extract the group name
									SignedToken st = (SignedToken)message.getObjContents().get(2);
									UserToken userToRemove = st.getToken();
									byte[] sig = st.getSignature();



									if(!userToRemove.verifyToken(sig, gsPubKey, GroupServer.TOKEN_VALID_PERIOD, sessionId, null)) {
										response = new Envelope("FAIL");
									}
									if(deleteUserFromGroup(username, groupname, userToRemove))
									{
										response = new Envelope("OK"); //Success
									}
								}
							}
						}
					}
					
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
				else if(message.getMessage().equals("DISCONNECT")) //Client wants to disconnect
				{
					socket.close(); //Close the socket
					proceed = false; //End this communication loop
				}
				else
				{
					response = new Envelope("FAIL"); //Server does not understand client request
					output.writeObject(new EncryptedEnvelope(response, sessionKey, snum)); snum++;

				}
			}while(proceed);	
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
		}
	}
	
	//Method to create tokens
	private UserToken createToken(String username, PublicKey fsServerPublicKey) 
	{
		//Check that user exists
		if (my_gs.userList==null) { // no users exist on the server
			return null;
		}
		if(my_gs.userList.checkUser(username))
		{
			//Issue a new token with server's name, user's name, and user's groups
			UserToken yourToken = new Token(my_gs.name, username, my_gs.userList.getUserGroups(username), new Timestamp(System.currentTimeMillis()),sessionId, fsServerPublicKey);
			return yourToken;
		}
		else
		{
			return null;
		}
	}
	
	// Create Keychain for a user
	private Keychain createKeychain(String username) {
		// Check that user exists
		if (my_gs.userList==null || !my_gs.userList.checkUser(username)) { // no users exist on the server or user does not exist
			return null;
		}
		
		// Get group keys for every group the user is in
		Keychain kc = new Keychain();
		System.out.println("should be printing keychainz");
		for (String group : my_gs.userList.getUserGroups(username)) {
			kc.setKeyList(group, my_gs.getGroupKeys(group)); // add group to Keychain and set key list
			System.out.println(group + ": " + kc.getMostRecentKey(group).hashCode());
		}
		
		return kc;
	}
	
	//Method to create a user
	private boolean createUser(String username, UserToken yourToken, byte[] pwd) throws NoSuchAlgorithmException
	{
		String requester = yourToken.getSubject();
		
		//Check if requester exists
		if(my_gs.userList.checkUser(requester))
		{
			//Get the user's groups
			ArrayList<String> temp = my_gs.userList.getUserGroups(requester);
			//requester needs to be an administrator
			if(temp.contains("ADMIN"))
			{
				//Does user already exist?
				if(my_gs.userList.checkUser(username))
				{
					return false; //User already exists
				}
				else
				{
					// Generate ranodm salt
					SecureRandom r = new SecureRandom();
					byte[] salt = new byte[128]; // make salt of size 128
					r.nextBytes(salt);
					
					// Set password for this user 
					byte[] hashedPwd = hashPassword(pwd, salt);
					
					// Add user
					my_gs.userList.addUser(username, hashedPwd, salt);
					return true;
				}
			}
			else
			{
				return false; //requester not an administrator
			}
		}
		else
		{
			return false; //requester does not exist
		}
	}
	
	private static byte[] hashPassword(byte[] pwd, byte[] salt) throws NoSuchAlgorithmException {
		// Hash password
		MessageDigest hash = MessageDigest.getInstance(HASH_ALGO);
		hash.update(pwd);
		hash.update(salt);
		byte[] hashedPwd = hash.digest(); 
		
		return hashedPwd;
	}
	
	//Method to delete a user
	private boolean deleteUser(String username, UserToken yourToken)
	{
		String requester = yourToken.getSubject();
		
		//Does requester exist?
		if(my_gs.userList.checkUser(requester))
		{
			ArrayList<String> temp = my_gs.userList.getUserGroups(requester);
			//requester needs to be an administer
			if(temp.contains("ADMIN"))
			{
				//Does user exist?
				if(my_gs.userList.checkUser(username))
				{
					//User needs deleted from the groups they belong
					ArrayList<String> deleteFromGroups = new ArrayList<String>();
					
					//This will produce a hard copy of the list of groups this user belongs
					for(int index = 0; index < my_gs.userList.getUserGroups(username).size(); index++)
					{
						deleteFromGroups.add(my_gs.userList.getUserGroups(username).get(index));
					}
					
					//Delete the user from the groups
					//If user is the owner, removeMember will automatically delete group!
					for(int index = 0; index < deleteFromGroups.size(); index++)
					{
						my_gs.userList.deleteUser(username);
					}
					
					//If groups are owned, they must be deleted
					ArrayList<String> deleteOwnedGroup = new ArrayList<String>();
					
					//Make a hard copy of the user's ownership list
					for(int index = 0; index < my_gs.userList.getUserOwnership(username).size(); index++)
					{
						deleteOwnedGroup.add(my_gs.userList.getUserOwnership(username).get(index));
					}
					
					//Delete owned groups
					for(int index = 0; index < deleteOwnedGroup.size(); index++)
					{
						//Use the delete group method. Token must be created for this action
						UserToken tempToken = new Token(my_gs.name, username, deleteOwnedGroup, new Timestamp(System.currentTimeMillis()), sessionId, null);
						//sign token
						deleteGroup(deleteOwnedGroup.get(index), tempToken);
					}
					
					//Delete the user from the user list
					my_gs.userList.deleteUser(username);
					
					return true;	
				}
				else
				{
					return false; //User does not exist
					
				}
			}
			else
			{
				return false; //requester is not an administer
			}
		}
		else
		{
			return false; //requester does not exist
		}
	}
	
	private boolean createGroup(String groupname, UserToken token) throws NoSuchAlgorithmException, NoSuchProviderException {
		String requester = token.getSubject();
		
		//Check if requester exists
		if(my_gs.userList.checkUser(requester))
		{
			//Get the requester's owned groups
			ArrayList<String> ownedGroups = my_gs.userList.getUserGroups(requester);
			// Check owned groups to make sure it has not already been created
			for (String g : ownedGroups) {
				if (g.equals(groupname)) { 
					System.out.println("error -- group already exists");
					return false; // group already exists
				}
			}
			
			if (my_gs.addGroup(requester, groupname)) { // add to list of all groups
				my_gs.userList.addOwnership(requester, groupname); // add to requester's list of owned groups
				my_gs.userList.addGroup(requester, groupname); // add to requester's list of groups they're in 
				return true;
			} else {
				System.out.println("error -- need unique groupname");
				return false; // group needs a unique name
			}
			
		}
		else {
			System.out.println("error -- requester does not exist");
			return false; //requester does not exist
		}	
	}
	
	private boolean deleteGroup(String groupname, UserToken token) {
		String requester = token.getSubject();
		
		//Check if requester exists
		if(my_gs.userList.checkUser(requester))
		{
			//Get the requester's owned groups
			ArrayList<String> ownedGroups = my_gs.userList.getUserGroups(requester);
			// Search owned groups for group to delete
			for (String g : ownedGroups) {
				if (g.equals(groupname)) { // if requester owns the group
					List<String> members = my_gs.groupList.get(groupname).listMembers(); // get all members in that group
					for (String m : members) { // remove all members from group
						my_gs.userList.removeGroup(m, groupname);
					}
					my_gs.userList.removeOwnership(requester, groupname); // remove group from requester's owned groups
					my_gs.removeGroup(requester, groupname); // remove group from list of all groups
					return true; 
				}
			}
			
			return false; // group was not found
		}
		else {
			return false; //requester does not exist
		}	
	}
	
	private List<String> listMembers(String group, UserToken token) {
		String requester = token.getSubject();
		
		//Check if requester exists
		if(my_gs.userList.checkUser(requester)) {
			// Get the requester's groups
			ArrayList<String> ownedGroups = my_gs.userList.getUserOwnership(requester);
			// Check if requester owns group
			for (String g : ownedGroups) {
				if (g.equals(group)) { // if requester is owner of the group
					List<String> users = new ArrayList<String>(my_gs.groupList.get(g).listMembers());
					return users;
				}
			}
			return null; // requester does not own the group			
		}
		return null; // requester does not exist
	}
	
	private boolean addUserToGroup(String user, String group, UserToken token) {
		String requester = token.getSubject();
		
		// Check if user to be added exists
		if (my_gs.userList.checkUser(user)==false) {
			System.out.println("error -- user does not exist");
			return false; // user does not exist
		}
		
		//Check if requester exists
		if(my_gs.userList.checkUser(requester)) {
			//Get the requester's groups
			ArrayList<String> ownedGroups = my_gs.userList.getUserOwnership(requester);
			// check all of requester's owned groups for group
			for (String g : ownedGroups) {
				if (g.equals(group)) { // if requester is owner of the group
					my_gs.userList.addGroup(user, g); // add group to user's list of groups
					my_gs.addUserToGroup(user, g); // add user to grouplist
					return true;
				}
			}
			System.out.println("error -- requester does not own the group or group does not exist");
			return false; // requester does not own the group
		}
		System.out.println("error -- requester does not exist");
		return false; //requester does not exist
	}
	
	private boolean deleteUserFromGroup(String user, String group, UserToken token) throws NoSuchAlgorithmException, NoSuchProviderException {
		String requester = token.getSubject();
		
		//Check if requester exists
		if(my_gs.userList.checkUser(requester)) {
			//Get the requester's groups
			ArrayList<String> ownedGroups = my_gs.userList.getUserOwnership(requester);
			// check all of requester's owned groups for group
			for (String g : ownedGroups) {
				if (g.equals(group)) { // if requester is owner of the group
					if (my_gs.groupList.get(g).checkMember(user)==false) { // if user does not exist in that group
						System.out.println("error -- user does not exist");
						return false; // user does not exist
					}
					
					my_gs.userList.removeGroup(user, g); // remove group from user's list of groups
					my_gs.deleteUserFromGroup(user, g); // remove user from group's list of members
					return true;
				}
			}
			
			System.out.println("error -- requester does not own the group");
			return false; // requester does not own the group
		}
		else {
			System.out.println("error -- requester does not exist");
			return false; //requester does not exist
		}
	}
	
}
