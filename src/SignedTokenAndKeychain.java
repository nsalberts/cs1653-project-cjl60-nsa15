public class SignedTokenAndKeychain implements java.io.Serializable {
	private SignedToken st;
	private Keychain kc;
	
	public SignedTokenAndKeychain(SignedToken s, Keychain k) {
		st = s;
		kc = k;
	}
	
	public SignedTokenAndKeychain(SignedTokenAndKeychain blah) {
		st = blah.getSignedToken();
		kc = blah.getKeychain();
	}
	
	public SignedToken getSignedToken() {
		return st;
	}
	
	public Keychain getKeychain() {
		return kc;
	}
}