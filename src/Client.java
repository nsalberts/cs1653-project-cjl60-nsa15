import java.net.Socket;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.io.*;
import java.util.*;
public abstract class Client {

	/* protected keyword is like private but subclasses have access
	 * Socket and input/output streams
	 */
	protected Socket sock;
	protected ObjectOutputStream output;
	protected ObjectInputStream input;
	protected SecretKey sessionKey;
	public PublicKey socketPubKey = null;
	protected int snum = 0;
	public boolean connect(final String server, final int port) {
		Security.addProvider(new BouncyCastleProvider()); 
		System.out.println("attempting to connect");
		//initialize socket 
		try {		
			sock = new Socket(server, port);
			output = new ObjectOutputStream(sock.getOutputStream());
			input = new ObjectInputStream(sock.getInputStream());

			//*********
			//secure send
			//*********
			output.writeObject("CONNECT");
			//parse public key
			PublicKey serverPubK = (PublicKey) input.readObject();
			socketPubKey = serverPubK;
			//generate AES key
			System.out.println("Server At "+ server+ ":"+ port+" has Key Fingerprint: " + serverPubK.getEncoded());

			//lookup dictionary of keyfiles
			//
			HashMap<String, PublicKey> spubkeys;
			try
			{
				FileInputStream fis = new FileInputStream("PubKeyDB.dat");
				ObjectInputStream fileStream = new ObjectInputStream(fis);
				spubkeys = (HashMap<String, PublicKey>)fileStream.readObject();
			}
			catch(FileNotFoundException e)
			{
				System.out.println("Server Public Key List Does Not Exist. Creating it now...");
				spubkeys = new HashMap<String, PublicKey>();
				
			}
			//if the server has been seen before
			if(spubkeys.containsKey((server +"++" +port))){
				PublicKey remeberedPubKey = spubkeys.get((server +"++" +port));
				//if match
				if (remeberedPubKey.equals(serverPubK)){
					System.out.println("Public Key Fingerprint Match");
				}
				else{
					System.out.println("Public Key Fingerprint DOES NOT MATCH. Continue Y/n");
					//prompt continue
					Scanner sc = new Scanner(System.in);
					String s = sc.next();
					if (!s.equals("Y")){
						
						sock = null;
						return false;
					}
					else {
						System.out.println("Adding Key Fingerprint to database");
						spubkeys.put((server +"++" +port), serverPubK);

					}
				}
			}
			else{
				System.out.println("Public Key Fingerprint has not been seen. Continue Y/n");
					//prompt continue
					Scanner sc = new Scanner(System.in);
					String s = sc.next();
					if (!s.equals("Y")){
						
						sock = null;
						return false;
					}
					else {
						System.out.println("Adding Key Fingerprint to database");
						spubkeys.put((server +"++" +port), serverPubK);

					}
			}
			try {
				ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream("PubKeyDB.dat"));
				outStream.writeObject(spubkeys);
			} catch (IOException ex) {
				System.out.println("IO Error writing to Keys file");
			} 
			KeyGenerator keygen = KeyGenerator.getInstance("AES", "BC");
			keygen.init(128);
			sessionKey = keygen.generateKey();
			//encrypt it with server's pub key
			Cipher rsaEncrypt = Cipher.getInstance("RSA/ECB/OAEPPADDING", "BC");
			rsaEncrypt.init(Cipher.ENCRYPT_MODE, serverPubK);
			byte[] ciphertext = rsaEncrypt.doFinal(sessionKey.getEncoded());
			output.writeObject(ciphertext);

			//confirm working secure socket
			Envelope confEnvelope = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey, snum);
			snum++;
			Envelope confResponseEnv = new Envelope("CONFIRM");
			System.out.println(((Integer) confEnvelope.getObjContents().get(0)));
			System.out.println(((Integer) confEnvelope.getObjContents().get(1)));
			confResponseEnv.addObject(((Integer) confEnvelope.getObjContents().get(0)) ^ ((Integer) confEnvelope.getObjContents().get(1)));
			//Send return
			output.writeObject(new EncryptedEnvelope(confResponseEnv, sessionKey, snum));
			snum++;
			return true;
		}
		catch (Exception e){
		//if exception
			System.out.println("Failiure to initialize socket");
			System.out.println(e.getMessage());
			sock = null;
			return false; 
		}
	}
	public void sendEncryptedEnvelope(Envelope e) throws IOException{
		output.writeObject(new EncryptedEnvelope(e, sessionKey, snum));
		snum++;

	}
	public boolean isConnected() {
		if (sock == null || !sock.isConnected()) {
			return false;
		}
		else {
			return true;
		}
	}

	public void disconnect()	 {
		if (isConnected()) {
			try
			{
				Envelope message = new Envelope("DISCONNECT");
				output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
				snum++;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
			}
		}
	}
}
