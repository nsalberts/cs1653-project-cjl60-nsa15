/* This list represents the users on the server */
import java.util.*;
import java.io.Console;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

	public class UserList implements java.io.Serializable {
	
		/**
		 * 
		 */
		private static final long serialVersionUID = 7600343803563417992L;
		private Hashtable<String, User> list = new Hashtable<String, User>();
		
		
		public synchronized void addUser(String username, byte[] pwd, byte[] salt) throws NoSuchAlgorithmException
		{	
			User newUser = new User(pwd, salt);
			list.put(username, newUser);
		}
		
		private synchronized byte[] createPassword(byte[] salt) throws NoSuchAlgorithmException {
			// Set password
			char[] pw1;
			char[] pw2;
			Console console = System.console();
			
			do {
				System.out.print("Enter a password: ");
				pw1 = console.readPassword();
				System.out.print("Retype your password: ");
				pw2 = console.readPassword();
				if (!pw1.equals(pw2)) {
					System.out.println("Error -- passwords do not match. Please enter again.");
				}
			} while (Arrays.equals(pw1, pw2)); // make sure passwords match
			
			// Salt password
			byte[] temp = new String(pw1).getBytes();
			byte[] saltedPwd = new byte[temp.length + salt.length];
			System.arraycopy(temp, 0, saltedPwd, 0, temp.length);
			System.arraycopy(salt, 0, saltedPwd, temp.length, salt.length);
			
			// Hash password
			MessageDigest hash = MessageDigest.getInstance("SHA-265");
			hash.update(saltedPwd);
			byte[] hashedPwd = hash.digest(); 
			
			// return
			return hashedPwd;
		}
		
		public synchronized void deleteUser(String username)
		{
			list.remove(username);
		}
		
		public synchronized boolean checkUser(String username)
		{
			if(list.containsKey(username))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public synchronized boolean checkGroup(String username, String groupname) {
			ArrayList<String> g = getUserGroups(username);
			if (g!=null) {
				if (g.contains(groupname)) {
					return true;
				}
			}
			
			return false;
		}
		
		public synchronized ArrayList<String> getUserGroups(String username)
		{
			return list.get(username).getGroups();
		}
		
		public synchronized ArrayList<String> getUserOwnership(String username)
		{
			return list.get(username).getOwnership();
		}
		
		public synchronized void addGroup(String user, String groupname)
		{
			list.get(user).addGroup(groupname);
		}
		
		public synchronized void removeGroup(String user, String groupname)
		{
			list.get(user).removeGroup(groupname);
		}
		
		public synchronized void addOwnership(String user, String groupname)
		{
			list.get(user).addOwnership(groupname);
		}
		
		public synchronized void removeOwnership(String user, String groupname)
		{
			list.get(user).removeOwnership(groupname);
		}
		
		public synchronized byte[] getSalt(String user) {
			if (checkUser(user)==false) { return null; }
			return list.get(user).getSalt();
		}
		
		public synchronized boolean comparePassword(String user, byte[] challenge, byte[] compare) throws NoSuchAlgorithmException {
			return list.get(user).comparePassword(challenge, compare);
		}
		
	class User implements java.io.Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6699986336399821598L;
		private ArrayList<String> groups;
		private ArrayList<String> ownership;
		private byte[] hashedSaltedPwd;
		private byte[] salt;
		
		public User(byte[] pwd, byte[] s)
		{
			groups = new ArrayList<String>();
			ownership = new ArrayList<String>();
			hashedSaltedPwd = pwd;
			salt = s;
		}
		
		public ArrayList<String> getGroups()
		{
			return groups;
		}
		
		public ArrayList<String> getOwnership()
		{
			return ownership;
		}
		
		public void addGroup(String group)
		{
			groups.add(group);
		}
		
		public void removeGroup(String group)
		{
			if(!groups.isEmpty())
			{
				if(groups.contains(group))
				{
					groups.remove(groups.indexOf(group));
				}
			}
		}
		
		public void addOwnership(String group)
		{
			ownership.add(group);
		}
		
		public void removeOwnership(String group)
		{
			if(!ownership.isEmpty())
			{
				if(ownership.contains(group))
				{
					ownership.remove(ownership.indexOf(group));
				}
			}
		}
		
		public void setPassword(byte[] password, byte[] s) {
			hashedSaltedPwd = password;
			salt = s;
		}
		
		public byte[] getSalt() {
			return salt;
		}
		
		public boolean comparePassword(byte[] challenge, byte[] compare) throws NoSuchAlgorithmException {
			MessageDigest hash = MessageDigest.getInstance("SHA-256");
			hash.update(challenge);
			hash.update(hashedSaltedPwd);
			byte[] result = hash.digest();
			
			return Arrays.equals(result, compare);
		}
	}
	
}	
