import java.io.*;
import java.util.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.PublicKey;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.IllegalBlockSizeException;

public class TestClient2 {
	
	public static void main(String args[]) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
				ShortBufferException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidAlgorithmParameterException {
		GroupClient gClient = new GroupClient();
		FileClient fClient = new FileClient();
		Scanner scan = new Scanner(System.in);
		UserToken token = null;
		byte[] tokenSig = null;
		Keychain keychain = null;
		PublicKey fspubkey = null;
		System.out.println("Starting test client...");
		System.out.println("TYPE 'list' TO LIST ALL COMMANDS ");
		String[] allCommands = "exit\t\tconnect\t\tdisconnect\tfsconnect\tfsdisconnect,getToken\tcreateUser\tdeleteUser\tcreateGroup\tdeleteGroup,addUserToGroup\tdeleteUserFromGroup\t\tlistMembers,fileup\t\tfiledown\tfiledel\t\tlistfiles".split(",");
		
		while(true) {
			System.out.print("\nEnter a command: "); 
			String command = scan.next().toLowerCase(); // get command
			String username;
			String groupname;
			List<String> members;
			
			if (command.equals("pathtest")) {
					String testPath = scan.next();
					System.out.print(new File(testPath).getAbsolutePath());
			}		
			else if (command.equals("exit")) {
					System.out.println("Disconnecting and exiting test client application....");
					gClient.disconnect();
					fClient.disconnect();
					System.exit(0);
			}		
			else if (command.equals("list")) {
					System.out.println("*** POSSIBLE COMMANDS ***");
					for (String tok : allCommands) {
						System.out.println(tok);
					}
			}
					
			else if (command.equals("connect")) {
					System.out.print("Enter group server name: "); 
					String server = scan.next(); // get server name
					System.out.print("Enter port number (default 8765): ");
					int port = scan.nextInt(); // get port number
					System.out.println("server: " + server + " port " + port);
					gClient.connect(server, port);
			}
					
			else if (command.equals("disconnect")) {
					gClient.disconnect();
			}
				
			else if (command.equals("fsconnect")) {
					System.out.print("Enter file server name: "); 
					String fserver = scan.next(); // get server name
					System.out.print("Enter port number (default 4321): ");
					int fport = scan.nextInt(); // get port number
					fClient.connect(fserver, fport);
					fspubkey=fClient.socketPubKey;
			}
					
			else if (command.equals("fsdisconnect")) {
					fClient.disconnect();
			}
			else if (command.equals("listfiles")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
			
					else{
						List<String> files  = fClient.listFiles(token, tokenSig);
						System.out.println("The files available to you on this server are:");
						for (String f: files){
							System.out.println(f);
						}
					}
			}
			else if (command.equals("fileup")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the name of the file to upload: ");
					String uPath = scan.next();
					System.out.print("Enter the name the file will be given on the server: ");
					String sPath = scan.next();
					System.out.print("Enter the group with which this file is to be shared: ");
					String gp = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					if (keychain != null && keychain.checkForGroup(gp)) {
						SecretKeySpec key = keychain.getMostRecentKey(gp);
						int keyId = keychain.getIdOfKey(gp, key);
						fClient.upload(uPath, sPath, gp, st, key, keyId);
					} else {
						System.out.println("Error -- could not find key for this group. Try getting a new token");
					}
					
			}
			else if (command.equals("filedown")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the name of the file to download: ");
					String sourcePath = scan.next();
					System.out.print("Enter the name the file will be given on your machine: ");
					String localPath = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					if (keychain != null) {
						fClient.download(sourcePath, localPath, st, keychain);
					} else {
						System.out.println("Error -- could not find key for this group. Try getting a new token");
					}
			}
			else if (command.equals("filedel")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the name of the file to delete: ");
					String fname = scan.next();
					
					SignedToken st = new SignedToken(token, tokenSig);
					fClient.delete(fname, st);
			}
			else if (command.equals("gettoken")) {
					if (!gClient.isConnected()) {
						System.out.println("Need to connect to a group server first");
						continue;
					}
					if (fspubkey==null){
						System.out.println("Need to connect to FileServer first");
						continue;
					}
					System.out.print("Enter your username: ");
					username = scan.next();
					Console con = System.console();
					System.out.print("Enter your password: ");
					char[] pwd = con.readPassword();
					
					SignedTokenAndKeychain stAndK = gClient.getToken(username, (new String(pwd)).getBytes(),fspubkey);
					if (stAndK==null) { 
						System.out.println("Could not authenticate");
						continue;
					}
					
					SignedToken st = stAndK.getSignedToken();
					if (st==null) { 
						System.out.println("Could not authenticate");
						continue;
					}
					token = st.getToken();
					tokenSig = st.getSignature();
					keychain = stAndK.getKeychain();

					if (token!=null && tokenSig!=null) {
						System.out.println("Token creation successful!");
					} else {System.out.println("!! Error creating token"); }
			}
					
			else if (command.equals("createuser")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter your desired username: ");
					username = scan.next();
					
					// Get password
					char[] pw1;
					char[] pw2;
					Console console = System.console();
						
					do {
						System.out.print("Enter a password: ");
						pw1 = console.readPassword();
						System.out.print("Retype your password: ");
						pw2 = console.readPassword();
						if (!Arrays.equals(pw1, pw2)) {
							System.out.println("Error -- passwords do not match. Please enter again.");
						}
					} while (!Arrays.equals(pw1, pw2)); // make sure passwords match
					SignedToken st = new SignedToken(token, tokenSig);
					if (gClient.createUser(username, st, (new String(pw1)).getBytes())) {
						System.out.println("User created!");
					} else { System.out.println("!! Error creating user"); }
			}
				
			else if (command.equals("deleteuser")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the user to delete: ");
					username = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					if (gClient.deleteUser(username, st)) {
						System.out.println("User deleted!");
					} else { System.out.println("!! Error deleting user"); }
			}
					
			else if (command.equals("creategroup")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter a name for the group: ");
					groupname = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					if (gClient.createGroup(groupname, st)) {
						System.out.println("Group created!");
					} else { System.out.println("!! Error creating group"); } 
			}
					
			else if (command.equals("deletegroup")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the name of the group to delete: ");
					groupname = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					if (gClient.deleteGroup(groupname, st)) {
						System.out.println("Group deleted!");
					} else { System.out.println("!! Error deleting group"); }
			}
				
			else if (command.equals("addusertogroup")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the name of the group: ");
					groupname = scan.next();
					System.out.print("Enter the name of the user you want to add: ");
					username = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					if (gClient.addUserToGroup(username, groupname, st)) {
						System.out.println(username + " added to " + groupname);
					} else { System.out.println("!! Error adding user to group"); }
			}
					
			else if (command.equals("deleteuserfromgroup")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the name of the group: ");
					groupname = scan.next();
					System.out.print("Enter the name of the user you want to delete: ");
					username = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					if (gClient.deleteUserFromGroup(username, groupname, st)) {
						System.out.println(username + " deleted from " + groupname);
					} else { System.out.println("!! Error deleting user from group"); }
			}
					
			else if (command.equals("listmembers")) {
					if (token==null) {
						System.out.println("You need a UserToken first");
						continue;
					}
					System.out.print("Enter the name of the group: ");
					groupname = scan.next();
					SignedToken st = new SignedToken(token, tokenSig);
					members = gClient.listMembers(groupname, st);
					if (members==null) {
						System.out.println("!! Error listing members"); 
						continue;
					}
					System.out.println("Members of " + groupname + ":");
					for (String m : members) {
						System.out.println(m);
					}
			}
			else {
					System.out.println("!! Error: invalid command");
			}
		} // end while-loop
		
	}
}