// Used to return both a token and its signature to clients

public class SignedToken implements java.io.Serializable {
	private UserToken token;
	private byte[] sig;
		
	public SignedToken(UserToken t, byte[] s) {
		token = t;
		sig = s;
	}
	
	public SignedToken(SignedToken st) {
		token = st.getToken();
		sig = st.getSignature();
	}
	
	public UserToken getToken() {
		return token;
	}
		
	public byte[] getSignature() {
		return sig;
	}
	
}