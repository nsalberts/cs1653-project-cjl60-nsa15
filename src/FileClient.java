/* FileClient provides all the client functionality regarding the file server */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import java.io.ObjectOutputStream;
import java.io.ByteArrayOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.IllegalBlockSizeException;

public class FileClient extends Client implements FileClientInterface {
	final static String SECURE_R_ALGO = "SHA1PRNG";
	
	public boolean delete(String filename, SignedToken token) {
		String remotePath;

		if (filename.charAt(0)=='/') {
			remotePath = filename.substring(1);
		}
		else {
			remotePath = filename;
		}
		Envelope env = new Envelope("DELETEF"); //Success
	    env.addObject(remotePath);
	    env.addObject(token);
	    try {
	    	output.writeObject(new EncryptedEnvelope(env, sessionKey, snum)); snum++;
			
		    env = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
		    
			if (env.getMessage().compareTo("OK")==0) {
				System.out.printf("File %s deleted successfully\n", filename);				
			}
			else {
				System.out.printf("Error deleting file %s (%s)\n", filename, env.getMessage());
				return false;
			}			
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	    	
		return true;
	}

	public boolean download(String sourceFile, String destFile, SignedToken token, Keychain kc) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
				ShortBufferException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidAlgorithmParameterException {
				if (sourceFile.charAt(0)=='/') {
					sourceFile = sourceFile.substring(1);
				}
			
				File file = new File(destFile);
			    try {
				    if (!file.exists()) {
				    	file.createNewFile();
					    FileOutputStream fos = new FileOutputStream(file);
					    
					    Envelope env = new Envelope("DOWNLOADF"); //Success
					    env.addObject(sourceFile);
					    env.addObject(token);
					    output.writeObject(new EncryptedEnvelope(env, sessionKey, snum)); snum++;
					
					    env = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++; // reads in first CHUNK msg
					    
						// get key ID, then use to get decryption key
						String groupName = (String)env.getObjContents().get(2); 
						int keyId = ((Integer)env.getObjContents().get(3)).intValue(); 
						SecretKeySpec key = kc.getKeyForGroup(groupName, keyId);
						
						// read in String as byte[], then use to recreate IV
						String s = (String)env.getObjContents().get(4);
						byte[] b = s.getBytes();
						IvParameterSpec iv = new IvParameterSpec(b);
						
						// Create Cipher and CipherOutputStream for decrypting the file 
						Cipher dCipher = Cipher.getInstance("AES/CBC/NoPadding");  // set decryption cipher modes
						dCipher.init(Cipher.DECRYPT_MODE, key, iv);
						CipherOutputStream cos = new CipherOutputStream(fos, dCipher); 
						
						while (env.getMessage().compareTo("CHUNK")==0) { 
								cos.write((byte[])env.getObjContents().get(0), 0, (Integer)env.getObjContents().get(1));
								System.out.printf(".");
								env = new Envelope("DOWNLOADF"); //Success
								output.writeObject(new EncryptedEnvelope(env, sessionKey, snum)); snum++;  
								env = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;								
						}										
						cos.close();
						fos.close();
						
						
					    if(env.getMessage().compareTo("EOF")==0) {
					    	 cos.close();
							 fos.close();
								System.out.printf("\nTransfer successful file %s\n", sourceFile);
								env = new Envelope("OK"); //Success
								output.writeObject(new EncryptedEnvelope(env, sessionKey, snum)); snum++;
						}
						else {
								System.out.printf("Error reading file %s (%s)\n", sourceFile, env.getMessage());
								file.delete();
								return false;								
						}
				    }    
					 
				    else {
						System.out.printf("Error couldn't create file %s\n", destFile);
						return false;
				    }
								
			
			    } catch (IOException e1) {
			    	
			    	System.out.printf("Error couldn't create file %s\n", destFile);
			    	return false;
			    
					
				}
			    catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				 return true;
	}

	@SuppressWarnings("unchecked")
	public List<String> listFiles(UserToken token, byte[] sig) {
		 try
		 {
			 Envelope message = null, e = null;
			 //Tell the server to return the member list
			 message = new Envelope("LFILES");
			 SignedToken st = new SignedToken(token, sig);
			 message.addObject(st); //Add requester's token
			 output.writeObject(new EncryptedEnvelope(message, sessionKey, snum)); snum++;
			 
			 e = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
			 
			 //If server indicates success, return the member list
			 if(e.getMessage().equals("OK"))
			 { 
				return (List<String>)e.getObjContents().get(0); //This cast creates compiler warnings. Sorry.
			 }
				
			 return null;
			 
		 }
		 catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return null;
			}
	}

	public boolean upload(String sourceFile, String destFile, String group,
			SignedToken token, SecretKeySpec key, int keyId) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
				ShortBufferException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidAlgorithmParameterException {
			
		if (destFile.charAt(0)!='/') {
			 destFile = "/" + destFile;
		 }
		
		try
		 {
			 
			 Envelope message = null, env = null;
			 //Tell the server to return the member list
			 message = new Envelope("UPLOADF");
			 message.addObject(destFile);
			 message.addObject(group);
			 message.addObject(token); //Add requester's token
			 message.addObject(keyId); //Add ID of key to be used
			 
			 // encrypt file before sending over to server; set up cipher for encryption
			 SecureRandom r = SecureRandom.getInstance(SECURE_R_ALGO); // create random number generator
			 Cipher eCipher = Cipher.getInstance("AES/CBC/NoPadding"); // set encryption cipher modes
			 eCipher.init(Cipher.ENCRYPT_MODE, key, r);
			 byte[] iv = eCipher.getIV(); // save IV for decryption
			 
			 // convert to string, add iv to the envelope and send 
			 message.addObject(new String(iv));
			 output.writeObject(new EncryptedEnvelope(message, sessionKey, snum)); snum++;
			
			 
			 // set up streams
			 FileInputStream fis = new FileInputStream(sourceFile);
			 CipherInputStream cis = new CipherInputStream(fis, eCipher);
			 env = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
			 
			 //If server indicates success, return the member list
			 if(env.getMessage().equals("READY"))
			 { 
				System.out.printf("Meta data upload successful\n");
				
			}
			 else {
				
				 System.out.printf("Upload failed: %s\n", env.getMessage());
				 return false;
			 }
			 
			
			 do {
				 byte[] buf = new byte[4096];
				 	if (env.getMessage().compareTo("READY")!=0) {
				 		System.out.printf("Server error: %s\n", env.getMessage());
				 		return false;
				 	}
				 	message = new Envelope("CHUNK");
					int n = cis.read(buf); //can throw an IOException
					
					if (n > 0) {
						System.out.printf(".");
					} else if (n < 0) {
						System.out.println("Read error");
						return false;
					}
					
					message.addObject(buf);
					message.addObject(new Integer(n));
					
					output.writeObject(new EncryptedEnvelope(message, sessionKey, snum)); snum++;
					
					
					env = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
										
			 }
			 while (fis.available()>0);	
			 
			 cis.close();
			 fis.close();
			 
			 
					 
			 //If server indicates success, return the member list
			 if(env.getMessage().compareTo("READY")==0)
			 { 
				
				message = new Envelope("EOF");
				output.writeObject(new EncryptedEnvelope(message, sessionKey, snum)); snum++;
				
				env = ((EncryptedEnvelope)input.readObject()).decrypt(sessionKey, snum); snum++;
				if(env.getMessage().compareTo("OK")==0) {
					System.out.printf("\nFile data upload successful\n");
				}
				else {
					
					 System.out.printf("\nUpload failed: %s\n", env.getMessage());
					 return false;
				 }
				
			}
			 else {
				
				 System.out.printf("Upload failed: %s\n", env.getMessage());
				 return false;
			 }
			 
		 }catch(Exception e1)
			{
				System.err.println("Error: " + e1.getMessage());
				e1.printStackTrace(System.err);
				return false;
				}
		 return true;
	}

}

