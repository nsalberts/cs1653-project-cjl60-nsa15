import java.util.Hashtable;
import java.util.ArrayList;
import javax.crypto.spec.SecretKeySpec;

public class Keychain implements java.io.Serializable {
	/* Each user has one Keychain that contains all of the keys for every group they are in */
	private Hashtable<String,ArrayList<SecretKeySpec>> groups; // group name -> history of keys for that group
	
	public Keychain() {
		groups = new Hashtable<String,ArrayList<SecretKeySpec>>();
	}
	
	public void addGroup(String group) {
		groups.put(group, new ArrayList<SecretKeySpec>());
	}
	
	public void addKey(String group, SecretKeySpec key) {
		groups.get(group).add(key);
	}
	
	public void setKeyList(String group, ArrayList<SecretKeySpec> keys) {
		if (groups.containsKey(group)) { // if group already exists, remove it
			groups.remove(group);
		}
		groups.put(group, keys);
	}
	
	public boolean checkForGroup(String group) {
		return groups.containsKey(group);
	}
	
	public SecretKeySpec getKeyForGroup(String group, int id) {
		return groups.get(group).get(id);
	}
	
	public SecretKeySpec getMostRecentKey(String group) {
		int numKeys = groups.get(group).size();
		return groups.get(group).get(numKeys-1);
	}
	
	public int getIdOfKey(String group, SecretKeySpec key) {
		return groups.get(group).indexOf(key);
	}
}