import java.util.List;
import java.sql.Timestamp;
import java.io.*;

import java.security.Key;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.NoSuchProviderException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Token implements UserToken, java.io.Serializable {
	private String user;
	private String issuer;
	private List<String> groups;
	private Timestamp timestamp;
	private String gSSessionId;
	private PublicKey fspubkey;
	
	public Token(String n_issuer, String n_user, List<String> n_groups, Timestamp t, String gsid, PublicKey fspk){
		user = n_user;
		issuer = n_issuer;
		groups = n_groups;
		timestamp = t;
		gSSessionId = gsid;
		fspubkey = fspk;
	}	
	/**
	*Returns the name of the issuer.
	*In this simple implementation, this is a basic getter
	*@return The issuer of the token
	*/	
	public String getIssuer(){
		return issuer;
	}
	/**
	*Returns the name of the user.
	*In this simple implementation, this is a basic getter
	*@return The user of the token
	*/
	public String getSubject(){
		return user;
	}
	/**
	*Returns the groups the users belong to.
	*In this simple implementation, this is a basic getter
	*@return The list of groups
	*/
	public List<String> getGroups(){
		return groups;
	}
	/**
	*Returns the time of token creation.
	*In this simple implementation, this is a basic getter
	*@return The timestamp of the token
	*/
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public String getGroupServerSessionID(){
		return gSSessionId;
	}
    
    public PublicKey getFileServerPubKeyFingerpring(){
    	return fspubkey;
    }
	public boolean verifyToken(byte[] signature, PublicKey pubKey, long validPeriod, String gssid, PublicKey fssid) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, NoSuchProviderException, IOException {		
		// Verify that the token was signed by the Group Server
		Signature s = Signature.getInstance("SHA256withRSA", "BC");
		s.initVerify(pubKey);

		// Convert this token to a byte array
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		ObjectOutputStream outStream = new ObjectOutputStream(bOut);
		outStream.writeObject(this);
		byte[] input = bOut.toByteArray();
		
		// Try to verify token
		s.update(input);
		if (s.verify(signature)==false) {
			// Could not verify token
			System.out.println("Failed to verify token's signature.");
			return false;
		}
		
		// Check if this token is still valid
		if ( (this.getTimestamp().getTime()+validPeriod) <= System.currentTimeMillis()) {
			System.out.println("Token is not valid anymore; get a fresh token");
			return false;
		}
		
		if ((gssid==null)&&(fssid==null)){
			return false;
		}
		else if (gssid!=null){
			if (!(gSSessionId.equals(gssid))){
				System.out.println("Bad SessionID");
				return false;
			}
		}
		else if (!(fssid.equals(fspubkey))){
			System.out.println("Token Not For This FileServer");

			return false;
		}

		return true;
	}
}
