import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
/* Driver program for FileSharing File Server */

public class RunFileServer {
	
	public static void main(String[] args) throws NoSuchProviderException, NoSuchAlgorithmException{
		String pubKeyFile = "GSPub.bin"; 
		
		// Open file with the group server's key, or if it does not exist, generate it
		try {
			ObjectInputStream keyStream = new ObjectInputStream(new FileInputStream(pubKeyFile));
			Object key = (Object)keyStream.readObject(); // just to make sure we can read from the file
		} catch(FileNotFoundException e) {
			System.out.println("Public Key File Does Not Exist. Generating keys...");
			GroupServer.getPublicKey();
		} catch(IOException e) {
			System.out.println("IO Error reading from public key file");
			e.printStackTrace();
			System.exit(-1);
		} catch(ClassNotFoundException e) {
			System.out.println("ClassNotFound Error reading from public key file");
			System.exit(-1);
		}
	
		if (args.length > 0) {		
			try {
				FileServer server = new FileServer(Integer.parseInt(args[0]), pubKeyFile);				
				server.start();
			}
			catch (NumberFormatException e) {
				System.out.printf("Enter a valid port number or pass no arguments to use the default port (%d)\n", FileServer.SERVER_PORT);
			}
		}
		else {
			FileServer server = new FileServer(pubKeyFile);
			server.start();
		}
	}

}
