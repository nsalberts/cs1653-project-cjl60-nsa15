/* Implements the GroupClient Interface */

import java.util.ArrayList;
import java.util.List;
import java.io.ObjectInputStream;
import java.security.PublicKey;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;

/** 	@brief Insecure implementation of GroupClientInterface.
	Group Client class. This class provides an 'insecure' implementation of 		   GroupClientInterface.
	@author Nicholas Alberts, Christine Lim, CS1653 Example code
	@date Feb 2015

*/
public class GroupClient extends Client implements GroupClientInterface {
 	/**
	Sends request for token to Group server
	@param username - username to authenticate
	@return UserToken object
	*/
	
	 public SignedTokenAndKeychain getToken(String username, byte[] pwd, PublicKey fsPubKey)
	 {
		try
		{
			SignedToken st = null;
			Keychain kc = null;
			Envelope message = null, response = null;
			
			//Tell the server to return a token.
			message = new Envelope("GET");
			message.addObject(username); //Add user name string
			message.addObject(fsPubKey);

			output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
			snum++;
		
			//Get the response from the server
			response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey, snum);
			snum++; 
			if(response.getMessage().equals("CHALLENGE-SALT")) {
				ArrayList<Object> temp = null;
				temp = response.getObjContents(); // should contain a challenge and the user's salt
						
				if(temp.size() == 2) {
					byte[] challenge = (byte[])temp.get(0);
					byte[] salt = (byte[])temp.get(1);
					
					// concat entered password || Salt and hash
					byte[] hashedSaltedPwd = hashBytes(pwd, salt);
					
					// concat challenge || hashed salted password and hash
					byte[] challengeResponse = hashBytes(challenge, hashedSaltedPwd);
					
					// send back to server
					message = new Envelope("CHALLENGE-RESPONSE");
					message.addObject(challengeResponse);
					output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
					snum++;
					// If successful response
					response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey, snum);
					snum++; 
					if(response.getMessage().equals("OK"))
					{
						//If there is a token and signature in the Envelope, return it 
						temp = null;
						temp = response.getObjContents();
						
						if(temp.size() == 2)
						{
							st = new SignedToken((SignedToken)temp.get(0));
							kc = (Keychain)temp.get(1);
							return new SignedTokenAndKeychain(st, kc);
						}
					} else {
						message = new Envelope("FAIL");
						message.addObject(null);
						output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
						snum++;
						output.reset();
						return null; // authentication failed
					}
				} else {
					// bad response from the server
					System.out.println("bad response from the server");
					message = new Envelope("FAIL");
					message.addObject(null);
					output.writeObject(new EncryptedEnvelope(message, sessionKey,snum));
					snum++;
					output.reset();
					return null; // authentication failed
				}
			} 
			
			
			return null;
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace(System.err);
			return null;
		}
		
	 }
	 
	 private static byte[] hashBytes(byte[] arr1, byte[] arr2) throws NoSuchAlgorithmException {
		// Combine and hash two byte arrays
		MessageDigest hash = MessageDigest.getInstance("SHA-256");
		hash.update(arr1);
		hash.update(arr2);
		byte[] hashed = hash.digest(); 
		
		return hashed;
	}
	 
	 /** Send Create user request to Server
	
	@param username - username to authenticate
	@param token authenticated user token
	@return boolean value if user can authenticate
	*/
	 public boolean createUser(String username, SignedToken token, byte[] pwd)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to create a user
				message = new Envelope("CUSER");
				message.addObject(username); //Add user name string
				message.addObject(token); //Add the requester's token
				message.addObject(pwd); //Add the entered password
				output.writeObject(new EncryptedEnvelope(message, sessionKey,snum));
				snum++;
				response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey,snum); 
				snum++;
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }	 
	
	/** Send delete user request to Server
	
	@param username - username to authenticate
	@param token authenticated user token
	@return boolean value if user can authenticate
	*/	 

	 public boolean deleteUser(String username, SignedToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
			 
				//Tell the server to delete a user
				message = new Envelope("DUSER");
				message.addObject(username); //Add user name
				message.addObject(token);  //Add requester's token
				output.writeObject(new EncryptedEnvelope(message, sessionKey,snum));
				snum++;
				response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey,snum);
				snum++; 
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	/** Create user group
	
	@param username - username to authenticate
	@param token  UserToken of current user
	@return boolean value if group can be created.
	*/	
	 
	 public boolean createGroup(String groupname, SignedToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to create a group
				message = new Envelope("CGROUP");
				message.addObject(groupname); //Add the group name string
				message.addObject(token); //Add the requester's token
				output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
				snum++;
				response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey,snum);
				snum++; 
				
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 /** Delete user group
	
	@param username - username to authenticate
	@param token  UserToken of current user
	@return boolean value if group can be deleted.
	*/	
	 public boolean deleteGroup(String groupname, SignedToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to delete a group
				message = new Envelope("DGROUP");
				message.addObject(groupname); //Add group name string
				message.addObject(token); //Add requester's token
				output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
				snum++;
				response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey, snum); 
				snum++;
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
	/** Get the list of users in a particular group
	
	@param group Name of group to query
	@param token  UserToken of current user.
	@return List of username strings in requested group. Returns NULL if no members are found
	*/	
	 @SuppressWarnings("unchecked")
	public List<String> listMembers(String group, SignedToken token)
	 {
		 try
		 {
			 Envelope message = null, response = null;
			 //Tell the server to return the member list
			 message = new Envelope("LMEMBERS");
			 message.addObject(group); //Add group name string
			 message.addObject(token); //Add requester's token
			 output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
			 snum++;
			 response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey, snum); 
			 snum++;
			 //If server indicates success, return the member list
			 if(response.getMessage().equals("OK"))
			 { 			
				return (List<String>)response.getObjContents().get(0); //This cast creates compiler warnings. Sorry.
			 }
				
			 return null;
			 
		 }
		 catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return null;
			}
	 }
	 
	 public boolean addUserToGroup(String username, String groupname, SignedToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to add a user to the group
				message = new Envelope("AUSERTOGROUP");
				message.addObject(username); //Add user name string
				message.addObject(groupname); //Add group name string
				message.addObject(token); //Add requester's token
				output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
				snum++;			
				response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey,snum); 
				snum++;
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }
	 
	 public boolean deleteUserFromGroup(String username, String groupname, SignedToken token)
	 {
		 try
			{
				Envelope message = null, response = null;
				//Tell the server to remove a user from the group
				message = new Envelope("RUSERFROMGROUP");
				message.addObject(username); //Add user name string
				message.addObject(groupname); //Add group name string
				message.addObject(token); //Add requester's token
				output.writeObject(new EncryptedEnvelope(message, sessionKey, snum));
				snum++;
				response = ((EncryptedEnvelope) input.readObject()).decrypt(sessionKey,snum);
				snum++; 
				//If server indicates success, return true
				if(response.getMessage().equals("OK"))
				{
					return true;
				}
				
				return false;
			}
			catch(Exception e)
			{
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace(System.err);
				return false;
			}
	 }

}
